# Delaunay Tetrahedralization

This project implements the [Bowyer-Watson algorithm](https://en.wikipedia.org/wiki/Bowyer-Watson_algorithm) to create a Delaunay tetrahedralization for a set of random points. This is equivalent to a standard Delaunay triangulation but extended to 3-dimensional space.

| Key | Command description |
|-----|-----------|
| Space | Create a new set of points and re-triangulate |
| P | Print current triangulation to the terminal |

![Example Delaunay Tetrahedralization](https://cdn.discordapp.com/attachments/990741694557327360/990741740078137364/Screenshot_from_2022-06-26_15-13-01.png)
