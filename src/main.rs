use macroquad::{
    input::{is_key_down, KeyCode},
    prelude::{
        clear_background, draw_line_3d, draw_sphere, next_frame, set_camera, vec3, Camera3D, BLACK,
        BLUE, DARKGRAY, GREEN, RED, WHITE,
    },
};
use rand::{thread_rng, Rng};
use std::cmp::{Eq, PartialEq};
use vek::{Mat3, Vec3};

const NUM_POINTS: u32 = 15;

#[derive(Debug)]
struct Tetrahedron {
    a: Vec3<f32>,
    b: Vec3<f32>,
    c: Vec3<f32>,
    d: Vec3<f32>,
    is_illegal: bool,
    circumcenter: Vec3<f32>,
    circumradius_squared: f32,
}

impl Tetrahedron {
    fn new(a: Vec3<f32>, b: Vec3<f32>, c: Vec3<f32>, d: Vec3<f32>) -> Self {
        let row1 = b - a;
        let ab_sqrd = a.distance_squared(b);
        let row2 = c - a;
        let ac_sqrd = a.distance_squared(c);
        let row3 = d - a;
        let ad_sqrd = a.distance_squared(d);
        let determinant = Mat3::new(
            row1.x, row1.y, row1.z, row2.x, row2.y, row2.z, row3.x, row3.y, row3.z,
        )
        .determinant();
        let multiplier = 0.5 * determinant.recip();

        let center_x = a.x
            + multiplier
                * Mat3::new(
                    ab_sqrd, row1.y, row1.z, ac_sqrd, row2.y, row2.z, ad_sqrd, row3.y, row3.z,
                )
                .determinant();
        let center_y = a.y
            + multiplier
                * Mat3::new(
                    row1.x, ab_sqrd, row1.z, row2.x, ac_sqrd, row2.z, row3.x, ad_sqrd, row3.z,
                )
                .determinant();
        let center_z = a.z
            + multiplier
                * Mat3::new(
                    row1.x, row1.y, ab_sqrd, row2.x, row2.y, ac_sqrd, row3.x, row3.y, ad_sqrd,
                )
                .determinant();
        let circumcenter = Vec3::new(center_x, center_y, center_z);
        let circumradius_squared = a.distance_squared(circumcenter);

        let mut vert_vec = vec![a, b, c, d];
        vert_vec.sort_unstable_by(|e, f| {
            e.magnitude_squared()
                .partial_cmp(&f.magnitude_squared())
                .unwrap()
        });
        Self {
            a: vert_vec[0],
            b: vert_vec[1],
            c: vert_vec[2],
            d: vert_vec[3],
            is_illegal: false,
            circumcenter,
            circumradius_squared,
        }
    }

    fn within_circumsphere(&self, v: Vec3<f32>) -> bool {
        let dist_sqrd = v.distance_squared(self.circumcenter);
        dist_sqrd <= self.circumradius_squared
    }

    fn contains_vertex(&self, v: Vec3<f32>) -> bool {
        (self.a - v).is_approx_zero()
            || (self.b - v).is_approx_zero()
            || (self.c - v).is_approx_zero()
            || (self.d - v).is_approx_zero()
    }
}

impl PartialEq for Tetrahedron {
    fn eq(&self, other: &Self) -> bool {
        ((self.a - other.a).is_approx_zero()
            || (self.a - other.b).is_approx_zero()
            || (self.a - other.c).is_approx_zero()
            || (self.a - other.d).is_approx_zero())
            && ((self.b - other.a).is_approx_zero()
                || (self.b - other.b).is_approx_zero()
                || (self.b - other.c).is_approx_zero()
                || (self.b - other.d).is_approx_zero())
            && ((self.c - other.a).is_approx_zero()
                || (self.c - other.b).is_approx_zero()
                || (self.c - other.c).is_approx_zero()
                || (self.c - other.d).is_approx_zero())
            && ((self.d - other.a).is_approx_zero()
                || (self.d - other.b).is_approx_zero()
                || (self.d - other.c).is_approx_zero()
                || (self.d - other.d).is_approx_zero())
    }
}

#[derive(Debug)]
struct Triangle {
    u: Vec3<f32>,
    v: Vec3<f32>,
    w: Vec3<f32>,
    is_illegal: bool,
}

impl Triangle {
    fn new(u: Vec3<f32>, v: Vec3<f32>, w: Vec3<f32>) -> Self {
        let mut vert_vec = vec![u, v, w];
        vert_vec.sort_unstable_by(|a, b| {
            a.magnitude_squared()
                .partial_cmp(&b.magnitude_squared())
                .unwrap()
        });
        Self {
            u: vert_vec[0],
            v: vert_vec[1],
            w: vert_vec[2],
            is_illegal: false,
        }
    }

    fn contains_vertex(&self, p: Vec3<f32>) -> bool {
        (p - self.u).is_approx_zero()
            || (p - self.v).is_approx_zero()
            || (p - self.w).is_approx_zero()
    }
}

impl PartialEq for Triangle {
    fn eq(&self, other: &Self) -> bool {
        ((self.u - other.u).is_approx_zero()
            || (self.u - other.v).is_approx_zero()
            || (self.u - other.w).is_approx_zero())
            && ((self.v - other.u).is_approx_zero()
                || (self.v - other.v).is_approx_zero()
                || (self.v - other.w).is_approx_zero())
            && ((self.w - other.u).is_approx_zero()
                || (self.w - other.v).is_approx_zero()
                || (self.w - other.w).is_approx_zero())
    }
}

impl Eq for Triangle {}

#[derive(Debug)]
struct Edge {
    u: Vec3<f32>,
    v: Vec3<f32>,
    is_illegal: bool,
}

impl Edge {
    fn new(u: Vec3<f32>, v: Vec3<f32>) -> Self {
        let mut vert_vec = vec![u, v];
        vert_vec.sort_unstable_by(|a, b| {
            a.magnitude_squared()
                .partial_cmp(&b.magnitude_squared())
                .unwrap()
        });
        Self {
            u: vert_vec[0],
            v: vert_vec[1],
            is_illegal: false,
        }
    }

    //fn contains_vertex(&self, p: Vec3<f32>) -> bool {
    //    (p - self.u).is_approx_zero() || (p - self.v).is_approx_zero()
    //}
}

impl PartialEq for Edge {
    fn eq(&self, other: &Self) -> bool {
        ((self.u - other.u).is_approx_zero() || (self.u - other.v).is_approx_zero())
            && ((self.v - other.u).is_approx_zero() || (self.v - other.v).is_approx_zero())
    }
}

impl Eq for Edge {}

#[derive(Default, Debug)]
struct Delaunay3D {
    _vertices: Vec<Vec3<f32>>,
    edges: Vec<Edge>,
    _triangles: Vec<Triangle>,
    _tetrahedra: Vec<Tetrahedron>,
}

impl Delaunay3D {
    fn new(vertices: &[Vec3<f32>]) -> Self {
        let mut tetrahedra = Vec::new();
        let last_index = vertices.len() - 1;
        let mut vertices = vertices.to_owned();
        vertices.sort_unstable_by(|a, b| a.x.partial_cmp(&b.x).unwrap());
        let min_x = vertices[0].x;
        let max_x = vertices[last_index].x;
        vertices.sort_unstable_by(|a, b| a.y.partial_cmp(&b.y).unwrap());
        let min_y = vertices[0].y;
        let max_y = vertices[last_index].y;
        vertices.sort_unstable_by(|a, b| a.z.partial_cmp(&b.z).unwrap());
        let min_z = vertices[0].z;
        let max_z = vertices[last_index].z;

        let dx = max_x - min_x;
        let dy = max_y - min_y;
        let dz = max_z - min_z;
        let max_delta = 2.0 * dx.max(dy).max(dz) + 1.0;
        let p0 = Vec3::new(min_x - 1.0, min_y - 1.0, min_z - 1.0);
        let p1 = Vec3::new(max_x + max_delta, min_y - 1.0, min_z - 1.0);
        let p2 = Vec3::new(min_x - 1.0, max_y + max_delta, min_z - 1.0);
        let p3 = Vec3::new(min_x - 1.0, min_y - 1.0, max_z + max_delta);

        if (p0 - p1).is_approx_zero()
            || (p0 - p2).is_approx_zero()
            || (p0 - p3).is_approx_zero()
            || (p1 - p3).is_approx_zero()
            || (p2 - p3).is_approx_zero()
        {
            panic!("Starting tetrahedron has duplicate points!");
        }
        let starting_tetrahedron = Tetrahedron::new(p0, p1, p2, p3);
        tetrahedra.push(starting_tetrahedron);

        for v in vertices.iter() {
            let mut triangles = Vec::new();
            for t in tetrahedra.iter_mut() {
                if t.within_circumsphere(*v) {
                    t.is_illegal = true;
                    triangles.push(Triangle::new(t.a, t.b, t.c));
                    triangles.push(Triangle::new(t.a, t.b, t.d));
                    triangles.push(Triangle::new(t.a, t.c, t.d));
                    triangles.push(Triangle::new(t.b, t.c, t.d));
                }
            }

            for i in 0..triangles.len() {
                for j in i + 1..triangles.len() {
                    if triangles[i].eq(&triangles[j]) {
                        triangles[i].is_illegal = true;
                        triangles[j].is_illegal = true;
                    }
                }
            }
            triangles.retain(|t| !t.is_illegal);
            tetrahedra.retain(|t| !t.is_illegal);

            for t in triangles.iter() {
                if !t.contains_vertex(*v) {
                    tetrahedra.push(Tetrahedron::new(t.u, t.v, t.w, *v));
                }
            }
        }

        tetrahedra.retain(|t| {
            !(t.contains_vertex(p0)
                || t.contains_vertex(p1)
                || t.contains_vertex(p2)
                || t.contains_vertex(p3))
        });

        let mut triangles = Vec::new();
        let mut edges = Vec::new();
        for t in tetrahedra.iter() {
            let abc = Triangle::new(t.a, t.b, t.c);
            let abd = Triangle::new(t.a, t.b, t.d);
            let acd = Triangle::new(t.a, t.c, t.d);
            let bcd = Triangle::new(t.b, t.c, t.d);

            let ab = Edge::new(t.a, t.b);
            let bc = Edge::new(t.b, t.c);
            let cd = Edge::new(t.c, t.d);
            let da = Edge::new(t.d, t.a);
            let ac = Edge::new(t.a, t.c);
            let bd = Edge::new(t.b, t.d);

            triangles.push(abc);
            triangles.push(abd);
            triangles.push(acd);
            triangles.push(bcd);

            edges.push(ab);
            edges.push(bc);
            edges.push(cd);
            edges.push(da);
            edges.push(ac);
            edges.push(bd);

            for i in 0..triangles.len() {
                for j in i + 1..triangles.len() {
                    if triangles[i].eq(&triangles[j]) {
                        triangles[i].is_illegal = true;
                        //triangles[j].is_illegal = true;
                    }
                }
            }
            triangles.retain(|e| !e.is_illegal);

            for i in 0..edges.len() {
                for j in i + 1..edges.len() {
                    if edges[i].eq(&edges[j]) {
                        edges[i].is_illegal = true;
                        //edges[j].is_illegal = true;
                    }
                }
            }
            edges.retain(|e| !e.is_illegal);
        }

        Self {
            _vertices: vertices.to_vec(),
            edges,
            _triangles: triangles,
            _tetrahedra: tetrahedra,
        }
    }
}

#[macroquad::main("Delaunay3D")]
async fn main() {
    let mut iteration = 0;
    let mut rng = thread_rng();
    let mut points = Vec::new();
    for _p in 0..NUM_POINTS {
        let x: f32 = (rng.gen::<f32>() * 0.8 + 0.1) * 20.0;
        let y: f32 = (rng.gen::<f32>() * 0.8 + 0.1) * 20.0;
        let z: f32 = (rng.gen::<f32>() * 0.8 + 0.1) * 15.0;
        points.push(Vec3::new(x, y, z));
    }
    let mut triangulation = Delaunay3D::new(&points);

    loop {
        clear_background(DARKGRAY);

        set_camera(&Camera3D {
            position: vec3(30.0, 10.0, 7.5),
            up: vec3(0.0, 0.0, 1.0),
            target: vec3(10.0, 10.0, 7.5),
            ..Default::default()
        });
        if is_key_down(KeyCode::Space) {
            iteration += 1;
            points = Vec::new();
            for _p in 0..NUM_POINTS {
                let x: f32 = (rng.gen::<f32>() * 0.8 + 0.1) * 20.0;
                let y: f32 = (rng.gen::<f32>() * 0.8 + 0.1) * 20.0;
                let z: f32 = (rng.gen::<f32>() * 0.8 + 0.1) * 15.0;
                points.push(Vec3::new(x, y, z));
            }
            triangulation = Delaunay3D::new(&points);
        }
        if is_key_down(KeyCode::P) {
            println!("triangulation:\n{:#?}", triangulation);
        }

        for p in points.iter() {
            draw_sphere(vec3(p.x, p.y, p.z), 0.5, None, WHITE);
        }
        for e in triangulation.edges.iter() {
            draw_line_3d(vec3(e.u.x, e.u.y, e.u.z), vec3(e.v.x, e.v.y, e.v.z), GREEN);
        }
        draw_line_3d(vec3(0.0, 0.0, 0.0), vec3(50.0, 0.0, 0.0), BLACK);
        draw_line_3d(vec3(0.0, 0.0, 0.0), vec3(0.0, 50.0, 0.0), BLACK);
        draw_line_3d(vec3(0.0, 0.0, 0.0), vec3(0.0, 0.0, 50.0), BLACK);
        draw_line_3d(vec3(0.0, 0.0, 0.0), vec3(2.0, 0.0, 0.0), RED);
        draw_line_3d(vec3(0.0, 0.0, 0.0), vec3(0.0, 2.0, 0.0), GREEN);
        draw_line_3d(vec3(0.0, 0.0, 0.0), vec3(0.0, 0.0, 2.0), BLUE);

        next_frame().await
    }
}
